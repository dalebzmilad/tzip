import smartpy as sp

# Import FA2 SmartPy template
FA2 = sp.io.import_script_from_url("https://smartpy.io/templates/FA2.py")

class TYPE:
    """
        Type Definitions
    """

    @staticmethod
    def transaction():
        """
            Transaction type used in "transfer_and_call" entrypoint
        """
        return sp.TRecord(
            amount      = sp.TNat,
            callback    = sp.TAddress,
            data        = sp.TBytes,
            to_         = sp.TAddress,
            token_id    = sp.TNat,
        ).right_comb()

    @staticmethod
    def transfer_and_call():
        """
            Entrypoint "transfer_and_call" input type
        """
        return sp.TList(
            sp.TRecord(
                from_   = sp.TAddress,
                txs     = sp.TList(TYPE.transaction())
            ).right_comb()
        )

    @staticmethod
    def callback():
        """
            Callback type used in "transfer_and_call" entrypoint
        """
        return sp.TRecord(
            amount      = sp.TNat,
            data        = sp.TBytes,
            receiver    = sp.TAddress,
            sender      = sp.TAddress,
            token_id    = sp.TNat,
        ).right_comb()


class FA2_transfer_and_call(FA2.FA2):
    """
        Extend FA2 implementation with a new entrypoint called "transfer_and_call"
    """

    @sp.sub_entry_point
    def _transfer(self, params):
        """
            Defines a global lambda that is used by both "transfer"
            and "transfer_and_call" entrypoints
        """
        super().transfer(params)

    @sp.entry_point
    def transfer(self, params):
        """
            Defines the "transfer" entrypoint
        """
        self._transfer(params)

    @sp.entry_point
    def transfer_and_call(self, batchs):
        """
            Defines the "transfer_and_call" entrypoint

            It performs a FA2 transfer and calls the `callback`
            address with some `data` of type bytes.
        """
        sp.set_type(batchs, TYPE.transfer_and_call())

        # Prepare FA2 transactions and callback operations
        transactions = sp.local("txs", sp.list([], t = FA2.Batch_transfer.get_transfer_type(self)))
        with sp.for_('batchs', batchs) as batch:
            with sp.for_('transaction', batch.txs) as tx:
                # Push FA2 transaction into the list
                transactions.value.push(
                    sp.record(
                        from_   = batch.from_,
                        txs     = [
                            sp.record(
                                to_         = tx.to_,
                                token_id    = tx.token_id,
                                amount      = tx.amount
                            )
                        ]
                    )
                )

                # Build callback operation
                callback = sp.contract(
                    TYPE.callback(),
                    tx.callback
                ).open_some("FA2_WRONG_CALLBACK_INTERFACE")
                args = sp.record(
                    amount      = tx.amount,
                    data        = tx.data,
                    receiver    = tx.to_,
                    sender      = batch.from_,
                    token_id    = tx.token_id,
                )
                sp.transfer(args, sp.tez(0), callback)

        # Apply FA2 transactions
        self._transfer(transactions.value)

class Receiver(sp.Contract):
    """
        A sample contract that implements the receiving side of `transfer_and_call` entrypoint
    """
    def __init__(self, fa2, from_, token_id, amount):
        self.init(
            amount          =   amount,
            fa2             =   fa2,
            from_           =   from_,
            received_data   =   sp.none,
            token_id        =   token_id
        )

    @sp.entry_point
    def default(self):
        sp.failwith("Not implemented")

    @sp.entry_point
    def receive(self, params):
        sp.set_type(params, TYPE.callback())
        sp.verify(sp.sender == self.data.fa2)
        sp.verify(params.sender == self.data.from_)
        sp.verify(params.receiver == sp.self_address)
        sp.verify(params.token_id == self.data.token_id)
        sp.verify(params.amount == self.data.amount)
        sp.verify(~self.data.received_data.is_some())
        self.data.received_data = sp.some(params.data)

class Sender(sp.Contract):
    def __init__(self, admin):
        self.init(admin = admin)

    @sp.entry_point
    def send(self, fa2, batchs):
        sp.verify(sp.sender == self.data.admin)
        fa2_contract = sp.contract(
            TYPE.transfer_and_call(),
            fa2,
            entry_point = "transfer_and_call"
        ).open_some("WRONG_INTERFACE")
        sp.transfer(batchs, sp.tez(0), fa2_contract)


@sp.add_test(name = "FA2 with transfer_and_call")
def test():
    fa2_admin = sp.test_account('admin')
    sender_admin = sp.test_account('sender_admin')

    config = FA2.FA2_config()
    metadata = sp.map({ "": sp.utils.bytes_of_string("") })

    fa2 = FA2_transfer_and_call(config, metadata, fa2_admin.address)

    sender = Sender(sender_admin.address)
    receiver = Receiver(fa2.address, sender.address, 0, 200)

    scenario = sp.test_scenario()
    scenario.h1("FA2 transfer and call")

    scenario.h2("Contracts")
    scenario.h3("FA2")
    scenario += fa2
    scenario.h3("Sender")
    scenario += sender
    scenario.h3("receiver")
    scenario += receiver

    scenario.h2("Admin mint some tokens")
    scenario += fa2.mint(
        address     = sender.address,
        amount      = 500,
        token_id    = 0,
        metadata    = sp.map({ "": sp.utils.bytes_of_string("") })
    ).run(sender = fa2_admin)

    scenario.h2("Sender transfer_and_call")

    callback = sp.to_address(receiver.typed.receive)
    txs = sp.record(
        to_         = receiver.address,
        callback    = callback,
        data        = sp.pack(sp.nat(42)),
        token_id    = 0,
        amount      = 200
    )
    batchs = sp.list([
        sp.record(
            from_   = sender.address,
            txs     = sp.list([txs])
        )
    ])
    scenario += sender.send(
        fa2     = fa2.address,
        batchs  = batchs
    ).run(sender=sender_admin)
